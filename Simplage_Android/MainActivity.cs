﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using System;

using Org.Opencv.Android;
using Org.Opencv.Core;
using Org.Opencv.Utils;
using Org.Opencv.Imgproc;
using Android.Graphics.Drawables;
using System.IO;
using Android.Content;

namespace Simplage_Android
{
    [Activity(Label = "Simplage_Android", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (!OpenCVLoader.InitDebug())
            {
                Console.WriteLine("Init OpenCV failed!!");
            }
            else
            {
                Console.WriteLine("Init OpenCV succefuly!!");
            }

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.loadImageButton);
            Button grayScaleBtn = FindViewById<Button>(Resource.Id.grayScaleButton);
            Button saveBtn = FindViewById<Button>(Resource.Id.saveButton);
            ImageView imageView = FindViewById<ImageView>(Resource.Id.imageView);

            button.Click += delegate
            {
                var imageIntent = new Intent();
                imageIntent.SetType("image/*");
                imageIntent.SetAction(Intent.ActionGetContent);
                StartActivityForResult(
                    Intent.CreateChooser(imageIntent, "Select photo"), 0);
            };

            //button.Click += delegate
            //{
            //    //button.Text = string.Format ("{0} clicks!", count++);

            //    SetImage();
            //};

            saveBtn.Click += delegate
            {
                BitmapDrawable bd = (BitmapDrawable)imageView.Drawable;
                Bitmap bitmap = bd.Bitmap;
                ExportBitmapAsJpeg(bitmap);
            };

            grayScaleBtn.Click += delegate
            {
                ConvertToGray();
            };
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                var imageView =
                    FindViewById<ImageView>(Resource.Id.imageView);
                imageView.SetImageURI(data.Data);
                if (imageView.Drawable != null)
                {
                    FindViewById<Button>(Resource.Id.grayScaleButton).Enabled = true;
                }
            }
        }

        void SetImage()
        {
            ImageView imageView = FindViewById<ImageView>(Resource.Id.imageView);


            using (Bitmap img = BitmapFactory.DecodeResource(Resources, Resource.Drawable.lena))
            {
                imageView.SetImageBitmap(img);
            }
            if (imageView.Drawable != null)
            {
                FindViewById<Button>(Resource.Id.grayScaleButton).Enabled = true;
            }
        }

        void ConvertToGray()
        {
            Mat loadedImage = new Mat();
            Mat grayImage = new Mat();

            ImageView imageView = FindViewById<ImageView>(Resource.Id.imageView);
            BitmapDrawable bd = (BitmapDrawable)imageView.Drawable;
            Bitmap bitmap = bd.Bitmap;
            Utils.BitmapToMat(bitmap, loadedImage);

            Imgproc.CvtColor(loadedImage, grayImage, Imgproc.ColorBgr2gray);
            using (Bitmap bm = Bitmap.CreateBitmap(grayImage.Cols(), grayImage.Rows(), Bitmap.Config.Argb8888))
            {
                Utils.MatToBitmap(grayImage, bm);

                imageView.SetImageBitmap(bm);
            }

            loadedImage.Release();
            grayImage.Release();
        }

        void ExportBitmapAsJpeg(Bitmap bitmap)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, "test.jpg");
            var stream = new FileStream(filePath, FileMode.Create);
            bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
            stream.Close();
        }
    }
}